import React, { useState } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import { useNavigate } from 'react-router-dom'

const Signup = () => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [role, setRole] = useState('Officer')
  const [error, setError] = useState('')
  const navigate = useNavigate()
  const apiUrl = process.env.REACT_APP_API_URL

  const handleSubmit = async (e) => {
    e.preventDefault()
    try {
      const { data } = await axios.post(`${apiUrl}/users/signup`, {
        username,
        password,
        role,
      })
      localStorage.setItem('username', data.username)
      localStorage.setItem('role', data.role)
      navigate('/')
    } catch (error) {
      setError(
        error.response.data.message || 'Error signing up, please try again.'
      )
      console.error('Error signing up:', error)
    }
  }

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-100">
      <div className="bg-white p-8 rounded-lg shadow-lg w-96">
        <h1 className="text-2xl font-bold mb-4">Sign Up</h1>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            className="mb-2 p-2 border border-gray-300 rounded w-full"
            required
          />
          <input
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            className="mb-2 p-2 border border-gray-300 rounded w-full"
            required
          />
          <select
            value={role}
            onChange={(e) => setRole(e.target.value)}
            className="mb-2 p-2 border border-gray-300 rounded w-full"
            required
          >
            <option value="Officer">Officer</option>
            <option value="Manager">Manager</option>
            <option value="Finance">Finance</option>
            <option value="Admin">Admin</option>
          </select>
          {error && <p className="text-red-500 mb-2">{error}</p>}
          <button
            type="submit"
            className="bg-blue-500 text-white py-2 px-4 rounded w-full"
          >
            Sign Up
          </button>
          <div className="mt-4 text-center">
            <p>
              Have an account?{' '}
              <Link to="/" className="text-blue-500">
                Sign In
              </Link>
            </p>
          </div>
        </form>
      </div>
    </div>
  )
}

export default Signup
