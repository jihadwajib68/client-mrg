import React, { useContext } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { AuthContext } from '../context/AuthContext'

const Header = () => {
  const { role, setRole } = useContext(AuthContext)
  const location = useLocation()

  const signoutHandler = () => {
    localStorage.removeItem('username')
    localStorage.removeItem('password')
    localStorage.removeItem('role')
    setRole('')
    window.location.href = '/'
  }

  return (
    <header className="bg-gray-800 text-white py-4 px-8 fixed w-full top-0 z-10">
      <div className="flex justify-between items-center">
        <Link to="/" className="text-2xl font-bold">
          Request App
        </Link>
        <nav>
          <ul className="flex space-x-4 items-center">
            {role && <li>Welcome, {role}</li>}
            {role ? (
              <li>
                <button
                  onClick={signoutHandler}
                  className="bg-red-500 text-white py-2 px-4 rounded"
                >
                  Sign Out
                </button>
              </li>
            ) : location.pathname === '/signup' ? (
              <li>
                <Link
                  to="/"
                  className="bg-blue-500 text-white py-2 px-4 rounded"
                >
                  Login
                </Link>
              </li>
            ) : (
              <li>
                <Link
                  to="/signup"
                  className="bg-blue-500 text-white py-2 px-4 rounded"
                >
                  Sign Up
                </Link>
              </li>
            )}
          </ul>
        </nav>
      </div>
    </header>
  )
}

export default Header
